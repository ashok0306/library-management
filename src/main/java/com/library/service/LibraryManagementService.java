package com.library.service;

import com.library.Model.User;
import com.library.exception.NoDataFoundException;
import com.library.exception.ResourceNotFoundException;
import com.library.repository.LibraryManagementRepositoryI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class LibraryManagementService implements LibraryManagementServiceI{

    @Autowired
    private LibraryManagementRepositoryI repositoryI;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Override
    public User saveUser(User user) {
        String password = user.getPassword();
        String encryptPWD = passwordEncoder.encode(password);
        user.setPassword(encryptPWD);
        return repositoryI.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return repositoryI.findAll();
    }

    @Override
    public User getUserById(long id) {
        Optional<User> user = repositoryI.findById(id);
        if(user.isEmpty())
            throw new NoDataFoundException("user id with " + id + " is not found");

        return user.get();
    }

    @Override
    public User updateUser(User user, long id) {
        User existingUser = repositoryI.findById(id).get();
        if(existingUser==null) {
            throw new ResourceNotFoundException("User", "id", id);
        } else {
            existingUser.setUserName(user.getUserName());
            repositoryI.save(existingUser);
            return existingUser;
        }
    }

    @Override
    public String deleteUser(long id) {
        repositoryI.deleteById(id);
        return id+" has been deleted successfully";
    }

    @Override
    public User getUserByEmail(String email) {
        return repositoryI.getUserByEmail(email);
    }

    @Override
    public User getUserByIdNative(long id) {
        return repositoryI.getUserByIdNative(id);
    }

}
