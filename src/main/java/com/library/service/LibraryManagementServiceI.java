package com.library.service;

import com.library.Model.User;

import java.util.List;

public interface LibraryManagementServiceI {
    User saveUser(User user);
    List<User> getAllUsers();
    User getUserById(long id);
    User updateUser(User user, long id);
    String deleteUser(long id);
    User getUserByEmail(String email);
    User getUserByIdNative(long id);


}
