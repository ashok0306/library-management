package com.library.service;

import com.library.Model.User;
import com.library.repository.LibraryManagementRepositoryI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserServiceDetails implements UserDetailsService {

    @Autowired
    private LibraryManagementRepositoryI repositoryI;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repositoryI.findByUserName(username);
        CustomUserDetails userDetails=null;
        if(user!=null) {
            userDetails = new CustomUserDetails();
            userDetails.setUser(user);
        }else{
            throw new UsernameNotFoundException("user not found with user name "+username );
        }
        return userDetails;
    }
}
