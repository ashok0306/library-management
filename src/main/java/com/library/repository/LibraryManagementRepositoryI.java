package com.library.repository;

import com.library.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LibraryManagementRepositoryI extends JpaRepository<User, Long> {

    // JPQL with index parameter
    @Query("select u from user u where u.email=?1")
    public User getUserByEmail(String email);

    //JPQL with named parameter
   /* @Query("select u from user u where u.email=:email")
    public User getUserByEmailNamedPara(@Param("email") String email);*/

    //Native SQL with named parameter
    @Query(value = "select * from user u where u.id=:id", nativeQuery = true)
    public User getUserByIdNative(@Param("id") long id);

    User findByUserName(String username);

}
