package com.library.repository;

import com.library.Model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LibraryManagementBookRepositoryI extends JpaRepository<Book, Integer> {



}
