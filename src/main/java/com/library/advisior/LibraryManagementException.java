package com.library.advisior;

import com.library.exception.ErrorObject;
import com.library.exception.NoDataFoundException;
import com.library.exception.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class LibraryManagementException {

    @ExceptionHandler
    public ResponseEntity<ErrorObject> handleResourceNotFoundException(ResourceNotFoundException re) {
        ErrorObject eo = new ErrorObject();
        eo.setMessage(re.getMessage());
        eo.setStatusCode(HttpStatus.NOT_FOUND.value());
        //eo.setTimestamp(System.currentTimeMillis());
        return new ResponseEntity<ErrorObject>(eo, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler
    public ResponseEntity<ErrorObject> handleNoDataFoundException(NoDataFoundException ne) {
        ErrorObject eo = new ErrorObject();
        eo.setMessage(ne.getMessage());
        eo.setStatusCode(HttpStatus.NOT_FOUND.value());
        eo.setTimestamp(System.currentTimeMillis());
        return new ResponseEntity<ErrorObject>(eo, HttpStatus.OK);
    }
}
