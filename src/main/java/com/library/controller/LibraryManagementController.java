package com.library.controller;

import com.library.Model.Book;
import com.library.Model.User;
import com.library.service.LibraryManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/library")
public class LibraryManagementController {
    @Autowired
    LibraryManagementService managementService;
    //@PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public User saveUser(@RequestBody User user) {

        return managementService.saveUser(user);
    }

    //@PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value = "/getall")
    public List<User> getAllUsers() {
        List<User> list = managementService.getAllUsers();
        System.out.println(list);
        return list;
    }

    @GetMapping("/getbyid/{id}")
    public User getUserById(@PathVariable long id) {
        return managementService.getUserById(id);
    }

    @PutMapping("/update/{id}")
    public User updateUser(@RequestBody User user, @PathVariable long id) {
        return managementService.updateUser(user,id);
    }
    @DeleteMapping("/delete/{id}")
    public String deleteUser(@PathVariable long id) {
        return managementService.deleteUser(id);
    }

    @GetMapping("/getbyemail/{email}")
    public User getUserByEmail(@PathVariable String email) {
        return managementService.getUserByEmail(email);
    }

    @GetMapping("/getnative/{id}")
    public List<Book> getUserByidNative(@PathVariable long id){
        return managementService.getUserByIdNative(id).getBookList();
    }



}
