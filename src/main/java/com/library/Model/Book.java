package com.library.Model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "book")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    @Id
    private int id;
    @Column
    private String title;
    @Column
    private int year;
    @Column
    private long isbn;
    @Column
    private int pages;
}
