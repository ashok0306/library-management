package com.library.controller;

import com.jayway.jsonpath.JsonPath;
import com.library.Model.Book;
import com.library.Model.Role;
import com.library.Model.User;
import com.library.service.LibraryManagementService;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@SpringBootTest(classes = {LibraryManagementController.class})
@AutoConfigureMockMvc
class LibraryManagementControllerTest {
    @Autowired
    WebApplicationContext context;
    private MockMvc mockMvc;
    @MockBean
    private LibraryManagementService service;
    @InjectMocks
    private LibraryManagementController controller;

    User user1 = new User(1,"ashok12", "ashok1234","ashok1234","ashok1234@gmail.com",
            new ArrayList<>(Arrays.asList(new Book(1,"spring", 2001,3456L, 300 ))),
            new HashSet<>(Arrays.asList(new Role())));

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void getUserById() throws Exception {
        User user = new User();
        Mockito.when(service.getUserById(1)).thenReturn(user);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/library/getbyid/7")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
    }

    @Test
    void getAllUsers() throws Exception {
        List<User> listUser = new ArrayList<>();
        User u = new User();
        u.setId(10l);
        u.setName("ashok kumar");
        listUser.add(u);
        Mockito.when(service.getAllUsers()).thenReturn(listUser);
      //  Assertions.assertEquals(1,service.getAllUsers().size());
        mockMvc.perform(MockMvcRequestBuilders
                .get("/library/getall")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

    }
}