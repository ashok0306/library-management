package com.library.service;

import com.library.Model.User;
import com.library.repository.LibraryManagementRepositoryI;
import org.hibernate.mapping.Any;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Scope("test")
class LibraryManagementServiceTest {

    @Autowired
    LibraryManagementService service;
    @MockBean
    LibraryManagementRepositoryI repositoryI;


    @Test
    void getAllUsers() {
        List<User> listUser = new ArrayList<>();
        User u = new User();
        u.setId(10l);
        u.setName("ashok kumar");
        listUser.add(u);
        Mockito.when(repositoryI.findAll()).thenReturn(listUser);
        Assertions.assertEquals(1, service.getAllUsers().size());
    }

    @Test
    void saveUser() {
        User u1 = new User();
        u1.setId(10L);
        u1.setName("ashok kumar");
        u1.setPassword("ashok");
        Mockito.when(repositoryI.save(u1)).thenReturn(u1);
        Assertions.assertEquals(u1, service.saveUser(u1));

    }

    @Test
    void getUserById() {
        User u1 = new User();
        u1.setId(10L);
        u1.setName("ashok kumar");
        u1.setPassword("ashok");
        Mockito.when(repositoryI.getById(10L)).thenReturn(u1);
        Assertions.assertEquals( u1, service.getUserById(10L).getName());
    }



}